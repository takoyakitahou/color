﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ColorList
{
    
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
         private void button1_Click(object sender,EventArgs e)
        {
            DialogResult dr = colorDialog1.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                textBox1.Text = ColorTranslator.ToHtml(colorDialog1.Color);
                textBox1.Text = colorDialog1.Color.ToString();
                textBox1.Text = string.Format("#{0:X2}{1:X2}{2:X2}", colorDialog1.Color.R, colorDialog1.Color.G, colorDialog1.Color.B);

                panel1.BackColor = colorDialog1.Color;
            }
        }
    }
}
